import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DataTableComponent } from './data-table/data-table.component';
import { FormComponent } from './form/form.component';


@NgModule({
  declarations: [
    FormComponent,
    DataTableComponent
  ],
  imports: [
    BrowserModule , 
    AppRoutingModule,
    HttpClientModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
